﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace QwiikAgency.Migrations
{
    /// <inheritdoc />
    public partial class changeMso : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ModifiedAt",
                table: "tokens");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "tokens");

            migrationBuilder.DropColumn(
                name: "ModifiedAt",
                table: "customers");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "customers");

            migrationBuilder.DropColumn(
                name: "ModifiedAt",
                table: "appointments");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "appointments");

            migrationBuilder.DropColumn(
                name: "ModifiedAt",
                table: "agency_settings");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "agency_settings");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedAt",
                table: "tokens",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy",
                table: "tokens",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedAt",
                table: "customers",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy",
                table: "customers",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedAt",
                table: "appointments",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy",
                table: "appointments",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedAt",
                table: "agency_settings",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy",
                table: "agency_settings",
                type: "text",
                nullable: true);
        }
    }
}
