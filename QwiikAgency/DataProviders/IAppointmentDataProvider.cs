﻿using System;
using QwiikAgency.Models;

namespace QwiikAgency.DataProviders
{
	public interface IAppointmentDataProvider
	{

            Task<IEnumerable<Appointment>> GetAllAppointments();
            Task<Appointment> GetAppointmentById(Guid id);
            Task<Appointment> CreateAppointment(Appointment appointment);
            Task<Appointment> UpdateAppointment(Appointment appointment);
            Task<bool> DeleteAppointment(Guid id);
            Task<List<Appointment>> GetAppointmentsByDateTime(DateTime dateTime);
            Task<List<DateTime>> GetAvailableTimeSlots(DateTime appointmentDate);
            Task<bool> IsAppointmentAvailable(DateTime appointmentDateTime);

    }
}

