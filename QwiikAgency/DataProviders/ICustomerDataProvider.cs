﻿using System;
using QwiikAgency.BindingModels;
using QwiikAgency.Models;

namespace QwiikAgency.DataProviders
{
	public interface ICustomerDataProvider
	{
        public Task<ResponseViewModel<CustomerViewModel>> List(string search, int? status, int page, int limit);
        public Task<Customer> Insert(Customer insert);
        public Task<Customer> Update(Customer update);
        public void Delete(Customer delete);
    }
}

