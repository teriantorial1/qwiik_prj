﻿using System;
using QwiikAgency.Models;

namespace QwiikAgency.DataProviders
{
	public interface ITokenDataProvider
	{
        Task<Token> CreateToken(Token token);
        Task<Token> GetTokenById(Guid id);
        Task<IEnumerable<Token>> GetTokensByAppointmentId(Guid appointmentId);
        Task<bool> UpdateToken(Token token);
        Task<bool> DeleteToken(Guid id);
        public string GenerateToken(int length = 8);
    }
}

