﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.EntityFrameworkCore;
using QwiikAgency.Models;

namespace QwiikAgency.DataProviders.Collections
{
    public class TokenDataProvider : ITokenDataProvider
    {
        private readonly Infrastructure.AppContext _context;

        public TokenDataProvider(Infrastructure.AppContext context)
        {
            _context = context;
        }

        public async Task<Token> CreateToken(Token token)
        {
            try
            {
                token.CreatedAt = DateTime.Now;
                token.CreatedBy = "ADMIN";
                await _context.tokens.AddAsync(token);
                await _context.SaveChangesAsync();
                return token;
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to create token: {ex.Message}");
            }
        }

        public async Task<Token> GetTokenById(Guid id)
        {
            var token = await _context.tokens.FindAsync(id);
            return token;
        }

        public async Task<IEnumerable<Token>> GetTokensByAppointmentId(Guid appointmentId)
        {
            var tokens = await _context.tokens.Where(t => t.FK_AppointmentId == appointmentId).ToListAsync();
            return tokens;
        }

        public async Task<bool> UpdateToken(Token token)
        {
            try
            {
                _context.tokens.Update(token);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to update token: {ex.Message}");
            }
        }

        public async Task<bool> DeleteToken(Guid id)
        {
            var token = await _context.tokens.FindAsync(id);
            if (token == null)
                return false;

            try
            {
                _context.tokens.Remove(token);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to delete token: {ex.Message}");
            }
        }

        public string GenerateToken(int length = 8)
        {
          Random _random = new Random();
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            char[] tokenChars = new char[length];

            for (int i = 0; i < length; i++)
            {
                tokenChars[i] = chars[_random.Next(chars.Length)];
            }

            return new string(tokenChars);
        }
    }
}
