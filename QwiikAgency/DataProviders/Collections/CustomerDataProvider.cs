﻿using System;
using System.Collections.Generic;
using System.Data;
using Dapper;
using QwiikAgency.BindingModels;
using QwiikAgency.Helpers;
using QwiikAgency.Models;

namespace QwiikAgency.DataProviders.Collections
{

	public class CustomerDataProvider : ICustomerDataProvider
    {
        private readonly Infrastructure.AppContext _context;
        private readonly IDbConnection _connection;

        public CustomerDataProvider(Infrastructure.AppContext context, IDbConnection connection)
        {
            _context = context;
            _connection = connection;
        }

        public async Task<ResponseViewModel<CustomerViewModel>> List(string search, int? status, int page, int limit)
        {
            ResponseViewModel<CustomerViewModel> result = new ResponseViewModel<CustomerViewModel>();

            var filter = "";
            int totalData = 0;
            int totalPage = 0;
            PaginationHelper pagination = new PaginationHelper(page, limit);

            if (search != null)
                filter = search.ToLower();
            string sql = string.Empty;

            if (string.IsNullOrEmpty(search))
            {
                sql = "SELECT * from customer cu WHERE 1=1";
            }
            else
            {
                sql = @"
        SELECT * from customer cu
        WHERE LOWER(cu.""Name"") LIKE @Filter OR LOWER(cu.""Email"") LIKE @Filter";
            }


            sql += " ORDER BY cu.\"ModifiedAt\" DESC";


             
            var queryParameters = new
            {
                Filter = $"%{filter}%",
                Status = status
            };

            var query = await _connection.QueryAsync<CustomerViewModel>(sql, queryParameters);

            var resultData = query.ToList();
            totalData = resultData.Count;

            if (limit > 0)
            {
                resultData = resultData.Skip(pagination.CalculateOffset()).Take(limit).ToList();
                totalPage = (int)Math.Ceiling((double)totalData / limit);
            }
            else
            {
                totalPage = 1;
            }


            var metaData = new MetaViewModel()
            {
                TotalItem = totalData,
                TotalPages = totalPage
            };

            if (!resultData.Any())
            {
                result.StatusCode = 200;
                result.Message = ApplicationConstant.NO_CONTENT_MESSAGE;
            }
            else
            {
                result.StatusCode = 200;
                result.Message = ApplicationConstant.SUCCES_MESSAGE;
                result.Data = resultData;
                result.Meta = metaData;
            }

            return result;
        }

        public async Task<Customer> Insert(Customer insert)
        {
            try
            {
                insert.CreatedAt = DateTime.Now;
                insert.CreatedBy = "ADMIN";
                await _context.customers.AddAsync(insert);
                await _context.SaveChangesAsync();
                return insert;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.Message);
            }

        }

        public async Task<Customer> Update(Customer update)
        {
            try
            {
                _context.customers.Update(update);
                await _context.SaveChangesAsync();
                return update;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.Message);
            }

        }

        public async void Delete(Customer delete)
        {
            try
            {
                var data = _context.customers.Remove(delete);

                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.Message);
            }

        }
    }
}

