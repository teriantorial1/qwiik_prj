﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.EntityFrameworkCore;
using QwiikAgency.BindingModels;
using QwiikAgency.Helpers;
using QwiikAgency.Models;

namespace QwiikAgency.DataProviders.Collections
{
    public class AppointmentDataProvider : IAppointmentDataProvider
    {
        private readonly Infrastructure.AppContext _context;
        private readonly IDbConnection _connection;

        public AppointmentDataProvider(Infrastructure.AppContext context, IDbConnection connection)
        {
            _context = context;
            _connection = connection;
        }
        public async Task<ResponseViewModel<AppointmentViewModel>> List(string search, int? status, int page, int limit)
        {
            ResponseViewModel<AppointmentViewModel> result = new ResponseViewModel<AppointmentViewModel>();

            var filter = "";
            int totalData = 0;
            int totalPage = 0;
            PaginationHelper pagination = new PaginationHelper(page, limit);

            if (search != null)
                filter = search.ToLower();
            string sql = string.Empty;

            if (string.IsNullOrEmpty(search))
            {
                sql = "SELECT * from appointments cu WHERE 1=1";
            }
            else
            {
                sql = @"SELECT * from appointments cu WHERE 1=1";
            }

            if (status > -1)
            {
                sql += "AND cu.\"Status\" = @Status";
            }

            sql += " ORDER BY cu.\"ModifiedAt\" DESC";



            var queryParameters = new
            {
                Filter = $"%{filter}%",
                Status = status
            };

            var query = await _connection.QueryAsync<AppointmentViewModel>(sql, queryParameters);

            var resultData = query.ToList();
            totalData = resultData.Count;

            if (limit > 0)
            {
                resultData = resultData.Skip(pagination.CalculateOffset()).Take(limit).ToList();
                totalPage = (int)Math.Ceiling((double)totalData / limit);
            }
            else
            {
                totalPage = 1;
            }


            var metaData = new MetaViewModel()
            {
                TotalItem = totalData,
                TotalPages = totalPage
            };

            if (!resultData.Any())
            {
                result.StatusCode = 200;
                result.Message = ApplicationConstant.NO_CONTENT_MESSAGE;
            }
            else
            {
                result.StatusCode = 200;
                result.Message = ApplicationConstant.SUCCES_MESSAGE;
                result.Data = resultData;
                result.Meta = metaData;
            }

            return result;
        }

        public async Task<IEnumerable<Appointment>> GetAllAppointments()
        {
            var appointments = await _context.appointments.ToListAsync();
            return appointments;
        }

        public async Task<Appointment> GetAppointmentById(Guid id)
        {
            var appointment = await _context.appointments.FindAsync(id);
            return appointment;
        }

        public async Task<Appointment> CreateAppointment(Appointment appointment)
        {
            try
            {
                await _context.appointments.AddAsync(appointment);
                await _context.SaveChangesAsync();
                return appointment;
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to create appointment: {ex.Message}");
            }
        }

        public async Task<Appointment> UpdateAppointment(Appointment appointment)
        {
            try
            {
                _context.appointments.Update(appointment);
                await _context.SaveChangesAsync();
                return appointment;
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to update appointment: {ex.Message}");
            }
        }

        public async Task<bool> DeleteAppointment(Guid id)
        {
            var appointment = await _context.appointments.FindAsync(id);
            if (appointment == null)
                return false;

            try
            {
                _context.appointments.Remove(appointment);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to delete appointment: {ex.Message}");
            }
        }

        public async Task<List<Appointment>> GetAppointmentsByDateTime(DateTime dateTime)
        {
            return await _context.appointments
                .Where(a => a.AppointmentDate == dateTime)
                .ToListAsync();
        }

        public async Task<List<DateTime>> GetAvailableTimeSlots(DateTime appointmentDate)
        {
            var availableTimeSlots = new List<DateTime>();


            DateTime startTime = appointmentDate.Date.AddHours(ApplicationConstant.START_TIME); 
            DateTime endTime = appointmentDate.Date.AddHours(ApplicationConstant.END_TIME); 

            for (DateTime slotTime = startTime; slotTime < endTime; slotTime = slotTime.AddMinutes(10))
            {
                var existingAppointments = await GetAppointmentsByDateTime(slotTime);
                if (!existingAppointments.Any())
                {
                    availableTimeSlots.Add(slotTime);
                }
            }

            return availableTimeSlots;
        }

        public async Task<bool> IsAppointmentAvailable(DateTime appointmentDateTime)
        {
            var existingAppointments = await GetAppointmentsByDateTime(appointmentDateTime);

            return !existingAppointments.Any();
        }

    }
}
