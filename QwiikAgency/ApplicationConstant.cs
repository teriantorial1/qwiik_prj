﻿using System;
namespace QwiikAgency
{
	public class ApplicationConstant
	{
        public const string NO_CONTENT_MESSAGE = "No Content";
        public const string SUCCES_MESSAGE = "OK";
        public const string ERROR_MESSAGE = "Data Not Found";
        public const int START_TIME = 8; //Jam Mulai Bekerja / Dibuka Pendaftaran
        public const int END_TIME = 17; //Jam Selesai Bekerja
        public const int STATUS_ERROR_DATA_NOT_EXIST = 404;
        public const int STATUS_ERROR_DATA_NOT_VALID = 400;
        public const int STATUS_ERROR_DATA_EXISTED = 409;
        public const int STATUS_ERROR_INTERNAL_ERROR = 500;
        public const string ENDPOINT_FORMAT = "qwiik/api/[controller]/[action]";

    }
}

