﻿using System;
namespace QwiikAgency.Helpers
{
    public class PaginationHelper
    {
        private int _page;
        private int _limit;

        public PaginationHelper()
        {
        }

        public PaginationHelper(int page, int limit) : this()
        {
            Page = page;
            Limit = limit;
        }

        public int Page
        {
            set => _page = value;
            get => _page < 1 ? (_page = 1) : _page;
        }

        public int Limit
        {
            set => _limit = value;
            get => _limit < 1 ? (_limit = 1) : _limit > 100 ? (_limit = 100) : _limit;
        }

        public int CalculateOffset()
        {
            return (Page - 1) == 0 || (Page - 1) < 0 ? 0 : (Page - 1) * Limit;
        }
    }

}

