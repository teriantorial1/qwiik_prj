﻿using System;
using System.Text.RegularExpressions;

namespace QwiikAgency.Helpers
{
	public static class Validations
	{
        public static bool IsValidEmail(string email)
        {
            // Regular expression pattern for email validation
            string pattern = @"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$";

            // Check if the email matches the pattern
            return Regex.IsMatch(email, pattern);
        }
    }
}

