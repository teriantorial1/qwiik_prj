﻿using System;
using Microsoft.EntityFrameworkCore;
using QwiikAgency.Models;

namespace QwiikAgency.Infrastructure
{
	public class AppContext : DbContext
	{
		public AppContext(DbContextOptions<AppContext> options) : base(options)
		{

		}

        public DbSet<Customer> customers { get; set; }
        public DbSet<Appointment> appointments { get; set; }
        public DbSet<Token> tokens { get; set; }
		public DbSet<AgencySettings> agency_settings { get; set; }
    }
}

