﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using QwiikAgency.BindingModels;
using QwiikAgency.BusinessProviders;
using QwiikAgency.Models;


namespace QwiikAgency.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AppointmentController : Controller
    {
        private readonly IAppointmentBusinessProvider _appointmentBusinesProvider;

        public AppointmentController(IAppointmentBusinessProvider appointmentBusinesProvider)
        {
            _appointmentBusinesProvider = appointmentBusinesProvider;
        }


        [HttpPost]
        public async Task<IActionResult> CreateAppointment([FromBody] AppointmentInsertModel param)
        {
            var appointmentDate = DateTime.Now;
            var customerData = param.Customer;

            var result = await _appointmentBusinesProvider.CreateAppointment(param.Customer, appointmentDate);
            return StatusCode(result.StatusCode, result);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateAppointment(Guid appointmentId, [FromBody] DateTime newAppointmentDateTime)
        {
            var result = await _appointmentBusinesProvider.UpdateAppointment(appointmentId, newAppointmentDateTime);
            return StatusCode(result.StatusCode, result);
        }


    }
}



