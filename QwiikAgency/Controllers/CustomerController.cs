﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using QwiikAgency.BusinessProviders;
using QwiikAgency.Models;

namespace QwiikAgency.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerBusinessProvider _customerBusinessProvider;

        public CustomerController(ICustomerBusinessProvider customerBusinessProvider)
        {
            _customerBusinessProvider = customerBusinessProvider;
        }

        [HttpGet]
        public async Task<IActionResult> ListCustomers(string search, int? status, int page = 1, int limit = 10)
        {
            var result = await _customerBusinessProvider.List(search, status, page, limit);

            if (result.StatusCode == 200)
            {
                return Ok(result);
            }
            else
            {
                return StatusCode(result.StatusCode, result.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> InsertCustomer([FromBody] Customer insert)
        {
            try
            {
                var result = await _customerBusinessProvider.Insert(insert);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCustomer(Guid id, [FromBody] Customer update)
        {
            try
            {
                update.Id = id; 
                var result = await _customerBusinessProvider.Update(update);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            try
            {
                var customerToDelete = new Customer { Id = id }; // Create a customer object with the provided ID
                await _customerBusinessProvider.Delete(customerToDelete);
                return Ok("Customer deleted successfully");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }
    }
}
