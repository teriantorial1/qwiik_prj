﻿using System;
using QwiikAgency.Models;

namespace QwiikAgency.BindingModels
{
	public class AppointmentInsertModel
	{
        public Customer Customer { get; set; }
        public DateTime AppointmentDateTime { get; set; }

    }
}

