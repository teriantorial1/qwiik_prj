﻿using System;
using QwiikAgency.Models;

namespace QwiikAgency.BindingModels
{
	public class AppointmentViewModel : BaseModel
	{
        public Guid FK_CustomerId { get; set; }
        public DateTime AppointmentDate { get; set; }
        public int Status { get; set; }
    }
}

