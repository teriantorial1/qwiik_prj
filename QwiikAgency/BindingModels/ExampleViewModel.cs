﻿using System;
namespace QwiikAgency.BindingModels
{
    public partial class ExampleViewModel
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedAt { get; set; }

    }

}