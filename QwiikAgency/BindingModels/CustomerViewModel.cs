﻿using System;
using QwiikAgency.Models;

namespace QwiikAgency.BindingModels
{
	public class CustomerViewModel : BaseModel
	{
        public string Name { get; set; }
        public string Email { get; set; }
        public string? PhoneNumber { get; set; }
        public List<Appointment> Appointments { get; set; }
    }
}

