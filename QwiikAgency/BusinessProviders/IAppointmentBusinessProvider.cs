﻿using System;
using QwiikAgency.BindingModels;
using QwiikAgency.Models;

namespace QwiikAgency.BusinessProviders
{
	public interface IAppointmentBusinessProvider
	{
        public Task<ResponseViewModel<Appointment>> CreateAppointment(Customer customer, DateTime appointmentDateTime);
        public Task<ResponseViewModel<Appointment>> UpdateAppointment(Guid appointmentId, DateTime newAppointmentDateTime);
    }
}

