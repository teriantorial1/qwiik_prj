﻿using System;
using QwiikAgency.BindingModels;
using QwiikAgency.Models;

namespace QwiikAgency.BusinessProviders
{
	public interface ICustomerBusinessProvider
	{
        public Task<ResponseViewModel<CustomerViewModel>> List(string search, int? status, int page, int limit);
        public Task<Customer> Insert(Customer insert);
        public Task<Customer> Update(Customer update);
        public Task Delete(Customer delete);
    }
}

