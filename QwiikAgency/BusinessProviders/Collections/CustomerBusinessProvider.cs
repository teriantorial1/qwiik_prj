﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using QwiikAgency.BindingModels;
using QwiikAgency.DataProviders;
using QwiikAgency.Helpers;
using QwiikAgency.Models;

namespace QwiikAgency.BusinessProviders.Collections
{
    public class CustomerBusinessProvider : ICustomerBusinessProvider
    {
        private readonly ICustomerDataProvider _customerDataProvider;

        public CustomerBusinessProvider(ICustomerDataProvider customerDataProvider)
        {
            _customerDataProvider = customerDataProvider;
        }

        public async Task<ResponseViewModel<CustomerViewModel>> List(string search, int? status, int page, int limit)
        {
            return await _customerDataProvider.List(search, status, page, limit);
        }

        public async Task<Customer> Insert(Customer insert)
        {

            return await _customerDataProvider.Insert(insert);
        }

        public async Task<Customer> Update(Customer update)
        {

            return await _customerDataProvider.Update(update);
        }

        public async Task Delete(Customer delete)
        {

             _customerDataProvider.Delete(delete);
        }
    }
}
