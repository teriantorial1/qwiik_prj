﻿using System;
using System.Collections.Generic;
using QwiikAgency.BindingModels;
using QwiikAgency.DataProviders;
using QwiikAgency.Helpers;
using QwiikAgency.Models;

namespace QwiikAgency.BusinessProviders.Collections
{
	public class AppointmentBusinessProvider : IAppointmentBusinessProvider
    {
        private readonly ITokenDataProvider _tokenDataProvider;
		private readonly IAppointmentDataProvider _appointmentDataProvider;
		private readonly ICustomerDataProvider _customerDataProvider;

        public AppointmentBusinessProvider(ITokenDataProvider tokenDataProvider, IAppointmentDataProvider appointmentDataProvider, ICustomerDataProvider customerDataProvider)
		{
			_appointmentDataProvider = appointmentDataProvider;
			_tokenDataProvider = tokenDataProvider;
			_customerDataProvider = customerDataProvider;

		}

        public async Task<ResponseViewModel<Appointment>> CreateAppointment(Customer customer, DateTime appointmentDateTime)
        {
            ResponseViewModel<Appointment> result = new ResponseViewModel<Appointment>();
            try
            {
                List<Appointment> listData = new List<Appointment>();

                #region insert customer
                if (!Validations.IsValidEmail(customer.Email))
                {
                    result.StatusCode = ApplicationConstant.STATUS_ERROR_DATA_NOT_VALID;
                    result.Message = "Email Not Valid";
                    return result;

                }

                var customerData = await _customerDataProvider.Insert(customer);
                #endregion insert customer

                #region insert appointment
                if (await _appointmentDataProvider.IsAppointmentAvailable(appointmentDateTime))
                {
                    result.StatusCode = ApplicationConstant.STATUS_ERROR_DATA_NOT_EXIST;
                    result.Message = "Appointment Date Not Available";
                    return result;
                }
                var appointment = new Appointment
                {
                    FK_CustomerId = customerData.Id,
                    AppointmentDate = appointmentDateTime,
                    Status = (int)AppointmentStatus.SCHEDULED
                };

                var createdAppointment = await _appointmentDataProvider.CreateAppointment(appointment);
                #endregion insert appointment

                #region issued token
                var token = new Token
                {
                    FK_AppointmentId = createdAppointment.Id,
                    TokenNumber = _tokenDataProvider.GenerateToken(),
                    Status = (int)TokenStatus.ISSUED
                };

                await _tokenDataProvider.CreateToken(token);
                #endregion issued token

                listData.Add(createdAppointment);
                result.Data = listData;
                result.Message = "Appointment inserted successfully";
                return result;
            }
            catch (Exception ex)
            {
                result.StatusCode = 500;
                result.Message = ApplicationConstant.ERROR_MESSAGE + "--"+ ex.InnerException.Message;
                throw;
            }
        }

        public async Task<ResponseViewModel<Appointment>> UpdateAppointment(Guid appointmentId, DateTime newAppointmentDateTime)
        {
            ResponseViewModel<Appointment> result = new ResponseViewModel<Appointment>();
            try
            {
                List<Appointment> listData = new List<Appointment>();
                var existingAppointment = await _appointmentDataProvider.GetAppointmentById(appointmentId);
                if (existingAppointment == null)
                {
                    result.StatusCode = ApplicationConstant.STATUS_ERROR_DATA_NOT_EXIST;
                    result.Message = "Appointment not found";
                    return result;
                }

                if (!await _appointmentDataProvider.IsAppointmentAvailable(newAppointmentDateTime))
                {
                    result.StatusCode = ApplicationConstant.STATUS_ERROR_DATA_NOT_EXIST;
                    result.Message = "New appointment date not available";
                    return result;
                }

                existingAppointment.AppointmentDate = newAppointmentDateTime;

                await _appointmentDataProvider.UpdateAppointment(existingAppointment);

                // Optionally, update associated token or other related tasks
                listData.Add(existingAppointment);
                result.Data = listData;
                result.Message = "Appointment updated successfully";
                return result;
            }
            catch (Exception ex)
            {
                result.StatusCode = 500;
                result.Message = ApplicationConstant.ERROR_MESSAGE + "--" + ex.InnerException.Message;
                return result;
            }
        }


    }
}

