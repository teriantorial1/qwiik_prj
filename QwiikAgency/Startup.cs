﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using QwiikAgency.BusinessProviders;
using QwiikAgency.BusinessProviders.Collections;
using QwiikAgency.DataProviders;
using QwiikAgency.DataProviders.Collections;
using QwiikAgency.Infrastructure;
using Npgsql;

namespace QwiikAgency
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            string connectionString = Configuration.GetConnectionString("DefaultConnection");

            services.AddDbContext<Infrastructure.AppContext>(options =>
                options.UseNpgsql(connectionString));

            services.AddControllers();
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "qwiik API", Version = "v1" });
            });

            // Register your services with the built-in ASP.NET Core DI container
            RegisterServices(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "qwiik API v1"));
            }

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        // Register your services with Autofac container
        public void ConfigureContainer(ContainerBuilder builder)
        {
            // Register your services with Autofac
            RegisterServices(builder);
        }

        // Register services with Autofac or ASP.NET Core DI container
        private void RegisterServices(IServiceCollection services)
        {
            // Register services with the built-in ASP.NET Core DI container
            services.AddScoped<IAppointmentBusinessProvider, AppointmentBusinessProvider>();
            services.AddScoped<ICustomerDataProvider, CustomerDataProvider>();
            services.AddScoped<IAppointmentDataProvider, AppointmentDataProvider>();
            services.AddScoped<ITokenDataProvider, TokenDataProvider>();
            services.AddScoped<ICustomerBusinessProvider, CustomerBusinessProvider>();
        }

        private void RegisterServices(ContainerBuilder builder)
        {
            // Register services with Autofac
            builder.RegisterType<CustomerBusinessProvider>().As<ICustomerBusinessProvider>();
            builder.RegisterType<AppointmentBusinessProvider>().As<IAppointmentBusinessProvider>();
            builder.RegisterType<CustomerDataProvider>().As<ICustomerDataProvider>();
            builder.RegisterType<AppointmentDataProvider>().As<IAppointmentDataProvider>();
            builder.RegisterType<TokenDataProvider>().As<ITokenDataProvider>();

            // Optionally, register other services with Autofac
        }
    }
}
