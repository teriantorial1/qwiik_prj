﻿using System;
namespace QwiikAgency.Models
{
	public class Appointment : BaseModel
	{
		public Guid FK_CustomerId { get; set; }
		public DateTime  AppointmentDate { get; set; }
		public int Status { get; set; }

	}

	public enum AppointmentStatus
	{
        AVAILABLE = 0,
        SCHEDULED = 1
	}
}

