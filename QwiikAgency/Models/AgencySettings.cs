﻿using System;
namespace QwiikAgency.Models
{
	public class AgencySettings : BaseModel
	{
		public DateTime OffDays { get; set; }
		public int MaxAppointmentsPerDay { get; set; }
	}
}

