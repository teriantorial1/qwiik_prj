﻿using System;
namespace QwiikAgency.Models
{
	public class Token : BaseModel
	{
		public Guid FK_AppointmentId { get; set; }
		public string TokenNumber { get; set; }
		public int Status { get; set; }

	}

	public enum TokenStatus
	{
		PENDING = 0,
		ISSUED = 1,
        CANCELED = 2,
        EXPIRED = 3

	}
}

