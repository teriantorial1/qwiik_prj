﻿
using System;
namespace QwiikAgency.Models
{
	public class Customer : BaseModel
	{
		public string Name { get; set; }
		public string Email { get; set; }
		public string? PhoneNumber { get; set; }
	}
}

