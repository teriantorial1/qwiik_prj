﻿using System;
using System.ComponentModel.DataAnnotations;

namespace QwiikAgency.Models
{
	public class BaseModel
	{
        [Key]
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public string? CreatedBy { get; set; } = string.Empty;

    }
}

